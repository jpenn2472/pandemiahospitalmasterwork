import React, { useState } from 'react';

import './App.css';
import { BrowserRouter } from 'react-router-dom';
import HeaderComponent from './components/ui/header.component';
import FooterComponent from './components/ui/footer.component';
import RecordDisplayComponent from './components/services/retrievedata.component';
import InputComponent from './components/ui/input.component';





const App: React.FC = () => {
 
  
  return (
   <React.Fragment>
     <div id= "app">
    <HeaderComponent></HeaderComponent>
    <RecordDisplayComponent></RecordDisplayComponent>
    <FooterComponent></FooterComponent>
       
     </div>
   </React.Fragment>
  )
}

export default App;