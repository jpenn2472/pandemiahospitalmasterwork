import React from 'react';
import { EmpData } from '../services/retrievedata.component';

interface EmpDisplayProps {
    empData: EmpData;
}

const RecordDisplayComponent: React.FC<EmpDisplayProps> = (props) => {
    return (
        <section id="emp-display-section">
            <h2 id="emp-name">{props.empData.empId} {props.empData.firstName} {props.empData.lastName}</h2>
            
        </section>
    )
}

export default RecordDisplayComponent;
