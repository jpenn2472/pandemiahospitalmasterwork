
import React, { useState } from 'react';
import Axios from 'axios';
import InputComponent from '../ui/input.component';
import RecordDisplayComponent from '../ui/recorddisplay.component'

export interface EmpData{
empId: number;
jobTitle: String;
firstName: String;
lastName: String;
phoneNum: number;
isInfected: boolean;
isTreatingInfected: boolean;
}

const RetrieveDataComponent: React.FC = () => {
   
    const [empData, setEmpData] = useState<EmpData>({
        empId: 0,
        jobTitle: 'Doctor',
        firstName: 'Jack',
        lastName: 'Doe',
        phoneNum: 555555,
        isInfected: false,
        isTreatingInfected: true
    });
    const [empArr, setEmpArr] = useState<EmpData[]>([empData]);

    

    const updateHistory = (employee: EmpData) => {
        if (empArr.some(e => e.empId === employee.empId)) {
          const arr = [employee, ...empArr.filter(e => e.empId !== employee.empId)];
          setEmpArr(arr);
        } else {
          // Copying array and updating state with new array
          const newArr = [employee, ...empArr];
          setEmpArr(newArr);
        }
      }
    

    const updateDisplayEmployee = (employee: EmpData) => {
        setEmpData(employee);
      }
      const processNewEmployeeData = (employee: EmpData) => {
        
        updateDisplayEmployee(employee);

        updateHistory(employee);

        
  }

  const userSubmitReceived = (str: string): void => {
    const url = `http:localhost:3000/employees/`;


    Axios.get(url)
    .then(response => {
      const empId = response.data.empId;

      const jobTitle = response.data.jobTitle;
      const firstName = response.data.firstName;
      const lastName = response.data.lastName;
      const phoneNum = response.data.phoneNum;
      const isInfected = response.data.isInfected;
      const isTreatingInfected = response.data.isTreatingInfected;

      processNewEmployeeData({ empId, jobTitle, firstName, lastName, phoneNum, isInfected, isTreatingInfected });

    }).catch(err => {
      console.log(err);
    });
}

return(
    
    <div>
        <RecordDisplayComponent empData = {empData}> </RecordDisplayComponent>
        <InputComponent userSubmitReceived={(str: string) => userSubmitReceived(str)}></InputComponent>
    </div>
);

}
export default RetrieveDataComponent;