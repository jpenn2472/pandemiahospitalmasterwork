package com.example.PanHospDemo.Controller;

import com.example.PanHospDemo.Model.Employee;
import com.example.PanHospDemo.Services.EmployeeService;
import org.hibernate.annotations.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/employees")
    public List<Employee> getAllEmployee() {
        return employeeService.getAllEmployees();
    }

    @CrossOrigin(origins = "http:localhost:3000")
    @GetMapping("/employees/{id}")
    public Optional<Employee> getByEmployeeId(@PathVariable("id") String id) {
        return employeeService.getByEmployeeId(id);
    }

    @PostMapping("/employees")
    public void addEmployee(@RequestBody Employee employee) {
        employeeService.addEmployee(employee);
    }
}
