package com.example.PanHospDemo.Services;

import com.example.PanHospDemo.Model.Employee;
import com.example.PanHospDemo.Model.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        //for(data_type item : collections) 
            for (Employee employee: employeeRepository.findAll()){
                employees.add(employee);
            }
            return employees;
    }

    public Optional<Employee> getByEmployeeId(String id) {
        return employeeRepository.findById(id);
    }

    public void addEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    public void updateUser(Employee employee, String id) {
        employeeRepository.save(employee);
    }

    public void deleteUser(String id) {
        employeeRepository.deleteById(id);
    }


}
