package com.example.PanHospDemo.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

//For this project I just marked Entity and Id annotations as I use React to compute calculations on client-side.D
@Entity
public class Employee {
    @Id
    private int empId;
    private String jobTitle;
    private String firstName;
    private String lastName;
    private int phoneNum;
    private boolean isInfected;
    private boolean isTreatingInfected;



public Employee() {

}
    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }

    public boolean isInfected() {
        return isInfected;
    }

    public void setInfected(boolean infected) {
        isInfected = infected;
    }

    public boolean isTreatingInfected() {
        return isTreatingInfected;
    }

    public void setTreatingInfected(boolean treatingInfected) {
        isTreatingInfected = treatingInfected;
    }

    public Employee(int empId, String jobTitle, String firstName, String lastName, int phoneNum, boolean isInfected, boolean isTreatingInfected) {
        this.empId = empId;
        this.jobTitle = jobTitle;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.isInfected = isInfected;
        this.isTreatingInfected = isTreatingInfected;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empId=" + empId +
                ", jobTitle='" + jobTitle + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNum=" + phoneNum +
                ", isInfected=" + isInfected +
                ", isTreatingInfected=" + isTreatingInfected +
                '}';
    }
}
