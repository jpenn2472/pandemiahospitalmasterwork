package com.example.PanHospDemo.Model;

//Must Needed package
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, String> {
}
