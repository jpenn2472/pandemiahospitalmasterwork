package com.example.PanHospDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanHospDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanHospDemoApplication.class, args);
	}

}
